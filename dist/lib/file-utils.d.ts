/**
 * File Utilities for generic purposes
 */
/**
* Resolve the default or custom template path
* @param defaultPath default template path (required and __dirname in most cases)
* @param templatePath custom templatre path (optional)
* @returns the resolved template path
*/
export declare function resolveTemplatePath(defaultPath: string, templatePath?: string): string;
/**
 * Extract the file name from a file path
 * @param filePath
 * @returns file name
 */
export declare function extractFileName(filePath: string): string;
/**
 * Chunk input text file by seperator string into one or more output files
 * @param inputFilePath location input text file
 * @param lineSeperator line seperator value
 * @param outputPath location seperated output files path
 */
export declare function chunkTxtFile(inputFilePath: string, lineSeperator: string, outputPath: string): void;
/**
 * Initialize file befote append to it be removing existing file and return resolved path
 * @param outputPath
 * @param outputFileNr
 * @param outputFileEnd
 * @returns resolved path to append file
 */
export declare function initAppendFilePath(outputPath: string, outputFileNr: number, outputFileEnd: string): string;
/**
 * Create destination directory (if not exist already) and copy relative source file
 * to destination directory (creates subdirectories in destiation directory based on
 * relative source file path)
 *
 * @param relFilePath relative source file path
 * @param destDir destination directory
 */
export declare function relativeCopyFile(relFilePath: string, destDir: string): void;
/**
 * Copy files from one folder to another folder (and create subdirectories if they don't exist)
 * @param pathFrom Folder to copy files from
 * @param pathTo Folder to copy files to
 */
export declare function copyFolder(pathFrom: string, pathTo: string): void;
/**
 * Remove folder with all contents of subfolders and files (if exists)
 *
 * @param folderPath Path to folder
 */
export declare function removeFolder(folderPath: string): void;
