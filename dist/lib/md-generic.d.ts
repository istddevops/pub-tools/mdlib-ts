/**
 * Generic Markdown related functions
 */
/**
 * Node Package Imports
 */
/**
 * Summary Content Line Section Data Interface
 */
export interface SummarySection {
    title: string;
    summaryLines: string[];
}
/**
 * Create a template to parse variable into
 * @param templateVar name of the variable to parse into the template (required)
 * @param startMark start marking string of the template (optional)
 * @param endMark end marking string of the template (optional)
 * @returns template string to parse variable into
 */
export declare function createTemplate(templateVar: string, startMark?: string, endMark?: string): string;
/**
 * Get first line (Header)
 * @param mdContentStr
 * @returns Md Header without spaces
 */
export declare function getMdHeaderStr(mdContentStr: string): string;
/**
 * Get all sections (Markdown HEADER LEVEL 2)
 * @param summaryMdStr
 * @returns sections array
 */
export declare function getSummarySections(summaryMdStr: string): SummarySection[];
/**
 * Get summary lines of a section content string
 * @param summarySectionMdStr
 * @returns summary lines array
 */
export declare function getSummaryLines(summarySectionMdStr: string): string[];
/**
 * Get summary line section id
 * @param summaryLine
 * @returns id name string
 */
export declare function extractSectionId(summaryLine: string): string;
/**
 * Get summary line link data include
 * @param summaryLine
 * @returns link string
 */
export declare function extractDataInclude(summaryLine: string): string;
/**
 * Extract content between start and end id
 * @param content
 * @param startId
 * @param endId
 * @returns extracted string
 */
export declare function extractData(content: string, startId: string, endId: string): string;
/**
 * Parse Image as Markdown Image Reference
 * @param imageId id or name of the image
 * @param imageRef link to the image
 * @returns Markdown Image Reference string
 */
export declare function parseMdImage(imageId: string, imageRef: string): string;
/**
 * Get markdown link reference from content
 * @param mdContent content string
 * @returns reference of the link (relative or URL)
 */
export declare function getMdContentLinkRef(mdContent: string): string;
/**
 * Get markdown link name from content
 * @param mdContent content string
 * @returns name of the link
 */
export declare function getMdContentLinkName(mdContent: string): string;
